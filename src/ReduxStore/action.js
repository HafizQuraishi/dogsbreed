
export const loading = () => {
    return {
        type: "LOADING"
    }
}

export const loadingImgaesAsync = (val) => {
    return { type: 'LOAD_PICS', breedList: val.res};
}

export const loadingImages = (val) => {
    return dispatch => {
        dispatch(loading());
        setInterval(() => {
            dispatch(loadingImgaesAsync(val))
        }, 3000);
    }
}