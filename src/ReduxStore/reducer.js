const initialState = {
    name: '',
    breedList: [],
    loading: false,
}
const BreedReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'STORE_BREED_NAME':
            return {
                name: action.name
            }
        case 'LOAD_PICS':
            return {
                breedList: action.breedList,
                loading: false
            }
        case 'LOADING':
            return {
                loading: true
            }
        default:
            return state;
    }
}

export default BreedReducer;