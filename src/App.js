import React from 'react';
import BreedList from './Components/BreedList';
import BreedPics from './Components/BreedPics';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// import './App.css';

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" exact component={BreedList} />
          <Route path="/images/:name" component={BreedPics} />
        </Switch>
      </div>
    </Router>
  );
}


export default App;
