import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { connect } from 'react-redux';

class BreedList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            breedList: [],
        }
    }

    async componentDidMount() {
        await axios.get('https://dog.ceo/api/breeds/list/all')
            .then((response) => {
                if (response.data.status === "success") {
                    this.setState(
                        this.state.breedList = response.data.message,
                    );
                }
            },
                (error) => {
                    console.log('error: ', error);
                });
    }

    render() {
        const listItems = Object.keys(this.state.breedList).map((value, index) => <li key={index} > <Link to={`images/${value}`} onClick={() => this.props.onSelectingBreedName({ value })}>{value}</Link> </li>);
        return (
            <div>
                <h1>Hello, Choose a Breed to view its Images</h1>
                <ul>
                    {listItems}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        breedList: state.list,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSelectingBreedName: (val) => dispatch({ type: 'STORE_BREED_NAME', name: val.value }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BreedList);