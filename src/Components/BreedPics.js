import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import * as action from '../ReduxStore/action';
import logo from "../logo.svg";



class BreedPics extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            breedList: [],
        }
    }
    
    async componentDidMount() {
        await axios.get(`https://dog.ceo/api/breed/${this.props.match.params.name}/images`)
            .then((response) => {
                if (response.data.status === "success") {
                    let res = response.data.message;
                    this.props.onLoadingPics({res});
                }
            },
            (error) => {
                console.log('error: ', error);
            });
    }

    render() {
        const picsItem = this.props.picsList? this.props.picsList.map((value) => <img src={`${value}`}></img>) : <img className="App-logo" src={logo}></img>;
        return (
            <div>
                {picsItem}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        picsList: state.breedList,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLoadingPics: (val) => dispatch(action.loadingImages(val)),
    }
}
// { type: 'LOAD_PICS', breedList: val.res}
export default connect(mapStateToProps, mapDispatchToProps)(BreedPics);